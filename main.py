import webapp2
from PIL import Image, ImageDraw, ImageFont
from random import randint, choice
import StringIO


class CloudHero(webapp2.RequestHandler):

    def get(self):
        width,height = 1000,500
        buzz = ['Cloud', 'Trasformation', 'Deployment',
                'Always-on', 'Enterprise', 'Open',
                'Containers', 'On-Premises', 'Agility',
                'IaaS', 'SaaS', 'Line-of-Business',
                'Public', 'Secure', 'Elastic',
                'Cognitive', 'Platform', 'Ecosystem']

        bg = Image.new('RGBA', (width, height), (32, 52, 72))
        layer = Image.new('RGBA', (width, height), 'gray')
        draw =ImageDraw.Draw(layer)

        for i in xrange(200):
            fnt = ImageFont.truetype('Roboto-Bold.ttf', size=randint(10, 30))
            draw.text((randint(0, width), randint(0, height)), choice(buzz), font=fnt, fill=(175, 175, 175, randint(75, 200)))

        bg.paste(layer, (0, 0), layer)
        out = StringIO.StringIO()
        bg.save(out, format='PNG')
        self.response.headers['content-type'] = 'image/png'
        self.response.out.write(out.getvalue())



app = webapp2.WSGIApplication([('/', CloudHero)], debug=False)
